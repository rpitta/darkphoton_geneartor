# darkphoton_geneartor

dark photon generator.

This generator is based on [DarkCapPy](https://github.com/agree019/DarkCapPy). The references for the calculation are [PPPC 4 DMν: A Poor Particle Physicist Cookbook for Neutrinos from DM annihilations in the Sun](https://arxiv.org/abs/1312.6408), [Dark Sunshine: Detecting Dark Matter through Dark Photons from the Sun](https://arxiv.org/abs/1602.01465) and [Dark Photons from the Center of the Earth: Smoking-Gun Signals of Dark Matter](https://arxiv.org/abs/1509.07525v3). 

In summary, using information about earth and it's elements, we wish to calculate the capture rate, the annihilation rate and with both information, the dark photon flux and event rate inside the detector. The event rate is the rate of decay of the dark photon $`A`$ in a paior of leptons ($`e^+ e^-`$ or $`\mu^+ \mu^-`$). Once the event rate is calculated, we can generate random points and directions for use in simulations.

The dark matter $`\chi`$, after being captured and thermalized, is non-relativistic in a way that it's knetic energy is usually negligible. The dark Photon $`A`$ usually be boosted and the pair of leptons from it's decay may be relativistic.

## Capture rate

The capture rate for each elemente will be:

```math
C^N_{cap} = n_\Chi (4\pi)^2 \int_0^{R} dr \int_0^{v_{gal}} du \int_{E_{min(u)}}^{E_{max(u)}} dE  r^2 u^2 n_N(r) f_{esc} (u) \frac{u^2 + v^2_{esc}(r)}{u} \frac{d\sigma_N}{dE_r} \Theta (\Delta E)
```

Where 
- $`N`$ is the element,
- $`n_\Chi = (0.3/cm^3)(GeV/m_\chi)`$ is the local dark matter density in the Milky way
- $`n_N(r)`$ the density of element N in the Earth ar raius r
- $`f_{esc}(u)`$ the dark matter velocity distribution in the Earth's reference frame
- $`v_{esc}(r)`$ the escape velocity of Earth at radius rate
- $`u `$ the velocity in the galaxy halo reference frame
- $` \Theta (\Delta E)`$ an step function to ensure that $` E_{max} - E_{min} > 0 `$ and the dark matter can be bound by earth potential well
and 
```math
 \frac{d\sigma_N}{dE_r} \approx 8 \pi^2 \epsilon^2 \alpha_\Chi \alpha Z^2_N \frac{1}{(u^2 + v^2_{esc})} \frac{m_N}{(2m_NE_r + m^2_A)^2} |F_N (E_r)|^2
 ```
is the cross section for dark matter to scatter off of a nucleus N. $` epsilon `$ is the knetic mixing, $` \alpha_\Chi = \frac{g^2_\Chi}{4\pi} `$ the dark matter fine structure, $` \alpha = \frac{e^2}{4\pi}`$ the eletromagnetic fine structure, $` m_n `$ the mass of the nucleus N, $` Z_n `$ it protons number, $` E_r `$ the energy recoil of the interaction, $` m_A `$ the dark photon mass and $`F_N (E_r)`$ the helm form factor. $`|F_N (E_r)|^2 = exp(-E_r/E_N) `$, where $` E_N = 0.114 GeV/A^{5/3}_N `$.

The min and max energy the dark matter $` \chi `$ can have are:
- $` E_min(u) = \frac{1}{2} m_\Chi u^2 `$ the knetic energy of free dark matter
- $` E_max(u) = \frac{2\mu^2_n}{m_n}(u^2 + v_{esc}^2)`$ the maximum kinematically accessible recoil energy allowed by elastic
scattering in the frame of the target nucleus 

we can aproximate $` \frac{1}{2m_nE_r + m^2_A} \longrightarrow \frac{1}{m_A^2} `$, since, for the masses considered, $`2m_NE_r  ~ O(10^{-5} GeV^2)`$ and $` m_A^2 ~ O(10^{-4} GeV^2) `$. This way. the integral become:

```math
C^N_{cap} =  (\frac{\epsilon^2}{m^4_A} \alpha_\Chi) \kappa_0(m_\Chi)

```
with
```math
\kappa_0 = n_\Chi 128 \pi^3 \alpha Z_n^2 \int_0^{R} dr \int_0^{v_{gal}} du \int_{E_{min(u)}}^{E_{max(u)}} r^2 u\, n_N(r) f_{esc}(u) m_N |F_N|^2

```
Obs: To make the process more efficient, we integrate u from 0 to the velocity where emin = emax


This make it easy to calculate the capture rate for multiples values in the $`(\epsilon, m_A)`$ parameter space.

From the reference, the capture rate is:
$`C^Fe_{cap} = 9.43e7 \approx 1.00e8`$
$`C^Ni_{cap} =  7.1e+06 `$

I've used aproximations for $`n_fe(r)`$ and $`f_esc(u)`$. The results I got, using the exact and the aproximation was:
- Complete fucntion: $`C^N_{cap} = 1.51e8`$
- kappa method: $`C^N_{cap} = 1.51e8`$

Both results are ~50% greather than the one at reference but it seems to be ok. The original study made the energy and velocity integration and them summed in discrete radius. Here we approximated the $`n_fe(r)`$ and integrated over it. 


For níquel, the kappa method results in:
- kappa method: $`C^Ni_{cap} = 1.11676e7`$

again, in the same order of magnitude

## annihilation rate

The annihilation rate for thermalized dark matter is:

```math

C_{ann} = <\sigma_{ann} v> \left[ \frac{G_N m_\Chi \rho_{earth}}{3T_{earth}} \right]^{3/2}

```

Where:
- $`<\sigma_{ann} v >` = (\sigma_{ann} v)_tree <S_s>$ the thermally-averaged cross section; 
- $`G_N`$ the gravitational constant;
- $`\rho_{earth}`$ the earth core's density;
- $`T_{earth}`$ the earth core's temperature;

The tree-level cross-section is:

```math
(\sigma_{ann} v)_tree = \frac{\pi \alpha^2_\Chi}{m^2_\Chi}\frac{[1 - m^2_A/m^2_\Chi]^3/2}{[1 - m^2_A/(2m^2_\Chi)]2}
```

and $`<S_s>`$ the thermalized averaged s-wave Sommerfeld enhancement. For this draft, it will be set to 1.

Once we have the annihilation rate, we calculate the equilibrium time. If the age of earth is equal or greahter than $`\tau = \frac{1}{\sqrt{C_{cap}C_{ann}}}`$, dark matter capture and annihilation has equilibred and the rate at which captured dark matter annihilates into dark photon is maximized.

## Signal rate

The dark photon decay in a pair of fermions with characteristic lenght L equals to:

```math
L = R_{earth}B \left( \frac{3.6e-9}{\epsilon} \right)^2 \left( \frac{m_\Chi/m_A}{1000} \right) \left( \frac{GeV}{m_A} \right)
```
Where B is the branching rate for the lepton decay and $`m_\Chi/m_A`$ is the Lorentz factor from the dark matter decay.

Lastly, the Signal Rate is:

```Math
    N_{sig} = 2 \Gamma_{ann} \frac{A_{eff}}{4\pi R^2_{earth}}\Epsilon_{decay} T
```
Where:
- $`\Gamma_{ann} = 1/2 C_{cap} tanh^2 \left( \frac{\tau_{earth}}{\tau} \right) `$
- T the time of detector data acquisiton, 
- $`A_{eff}`$ the effective area of the detector;
- $`D`$  the effective depth of the detector;
- $` \Epsilon_{decay} = exp(-R_{earth}/L) - exp(-(R_{earth} + D)/L)`$ the probability that the dark photon decays between the distance R and R + D.

For IceCube, I've used $´A_{eff} = 1km^2´$ and $´D = 1km´$
For COSINE-199, I've used $´A_{eff} = 2.4e-5 km^2´$ and $´D = 0,002 km´$

Setting B = 1 and using mA = [0.001, 0.01, 0.1, 1, 10, 100, 1000] and $`\epsilon`$ = [1e-6,1e-7,1e-8,1e-8,1e-9,1e-10,1e-11]. Using those discrete points, the $`N_{sig}`$ for the decay of dark photons in a e+e- pair was:

### IceCube

![IceCube](https://gitlab.com/rpitta/darkphoton_geneartor/-/raw/master/images/signal_icecube.png)

### COSINE-100

![Cosine](https://gitlab.com/rpitta/darkphoton_geneartor/-/raw/master/images/signal_cosine.png)
