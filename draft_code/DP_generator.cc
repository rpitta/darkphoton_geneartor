/** Code writing using as basis the DarCapPy code, 
###refs: PPPC 4 DMν: A Poor Particle Physicist Cookbook for Neutrinos from Dark Matter annihilations in the Sun
######## Dark Sunshine: Detecting Dark Matter through Dark Photons from the Sun
######## Dark Photons from the Center of the Earth: Smoking-Gun Signals of Dark Matter
**/

#include "TMinuit.h"
#include "TMath.h"
#include "TFile.h"
#include "TH1F.h"
#include "TF1.h"
#include "TTree.h"
#include "TRandom.h"


//Unit Conversions
Double_t amu2GeV(Double_t m){
    
    // 1 = 0.938272 GeV / Amu
    Double_t rvt =  0.938272 * m;
    
    return rvt;
}

Double_t amu2g(Double_t m)}{
    //1 = 1.66053892e-24 g / Amu
    Double_t rvt = 1.66053892e-24 * m;
    return rvt;
}

Double_t GeV2s(Double_t gev){
    //1 = 6.58e-25 GeV Sec
    Double_t rvt = 1.52e24 * gev;
    return rvt; 
}

Double_t s2GeV(Double_t s){
    //    1 = 6.58e-25 GeV Sec
    Double_t rvt = (6.58e-25)**-1 * s;
    return rvt;
}

Double_t GeV2cm(Double_t gev){
    //1 = 1.97e-14 GeV cm
    Double_t rvt = 5.06e13 * gev;
    return rvt;
}

Double_t cm2GeV(Double_t cm){
    //1 = 1.97e-14 GeV cm
    Double_t rvt = (0.197e-13)**-1 * cm;
    return rvt;
}

Double_t KeltoGeV(Double_t kelvin){
    //1 = 1.16e13 GeV / K
    Double_t rvt  = 8.62e-14 * kelvin;
    return rvt;
}

Double_t yr2s(Double_t year){
    // 1 = (365)(24)(3600) sec/yr
    Double_t rvt  = year*365*24*3600;
    return rvt;
}

Double_t g2GeV(Double_t m){
    //1 = 5.62e23 GeV / g
    Double_t rvt = (5.62e23) * m; // #GeV
    return rvt;
}

//For the calculation of the capture rate. 

Double_t formFactor(Double_t *x, Double_t *par){
    //Returns the form-factor squared of element N with recoil energy Energy in GeV
    Double_t E = par[0];
    Double_t element = x[0];
    Dobule_t E_N = 0.114/(element**(5./3));
    rvt = TMath::Exp(-E/E_N);
    return rvt;
}




//to test on root, change in the compilation or compile using root
//int main(int argc, char *argv[])
void DP_generator()
{
    //constants
    Double_t c = 3.0e10; // Speed of Light in cm/s
    Double_t G = 6.674e-11 * 100**3 * (1000)**-1; //Gravitational constant in cm^3/(g s^2)
    Double_t V_dot = 220.0e5/c; //Velocity of Sun relative to galactic center
    Double_t V_cross = 29.8e5/c; //Velocity of Planet relative to Sun
    Double_t V_gal = 550.0e5/c; //Escape velocity of the Galaxy
    Double_t u_0 = 245.0e5/c; //Characteristic speed of Galactic DM
    Double_t k = 2.5; //Kurtosis of velocity disribution
    Double_t RCross = 6.371e8; //Radius of Planet: cm

    //fo annihilation calculation
    Double_t Gnat = 6.71e-39; //ravitational constant G in natural units GeV^-2
    Double_t rhoCross = 5.67e-17;// Density at the center of Earth in GeV^4
    Double_t TCross = 4.9134e-10;// Temperature at the center of Earth in GeV
    Double_t tauCross = yr2s(4.5e9); // 4.5 Gyr -> sec; //Age of Earth in seconds
    std::cout<<"tauCross equals to"<<tauCross<<"seconds"<<endl;


    //fine structure ctes
    Double_t alpha = 1/137; //electromagnetic fine structure constants e^2/4pi
    Double_t alphax = 0.035;//fine structure constants g_X^2/4pi, g_X the dark U(1) coupling

    /*if(argc !=4){
        std::cout<<"Usage ./iDM_generator m1 m2 mdp gamma1 epsilon nevt";
            exit(1);
        }
    */

    /*input parameter
    mx = dark fermion mass GeV
    ma = dark photon mass GeV
    epsilon = kinematic mixing
    ntotal = evt number
    */
    /*Double_t mx=atof(argv[1]);
    Double_t ma=atof(argv[2]);
    Double_t epsilon=atof(argv[3]);
    Int_t ntotal = atoi(argv[4]);*/
    Double_t mx=1;
    Double_t ma=0.1;
    Double_t epsilon=10e-7;
    Int_t ntotal = 10000;


    //simple test to confirm if everthing is working as intended
    TF1 *Helm = new TF1("Helm",formFactor,1,20,1);
    Helm->SetNpx(10000);
    Helm->SetParameters(0.01);
    //remove the comment for compilation
    //return 1;
}
